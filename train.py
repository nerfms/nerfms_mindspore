# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
"""Buile and train model."""

import os
import time

import mindspore as md
import numpy as np
import mindspore.numpy as mnp
from engine import RendererWithCriterion, test_net, train_net
from tqdm import tqdm

from data import load_blender_data
from models import VolumeRenderer
from utils.config import get_config
from utils.engine_utils import context_setup, create_nerf
from utils.ray import generate_rays, get_rays
from utils.results_handler import save_image, save_video
from utils.sampler import sample_grid_2d



def train_pipeline(config, out_dir):
    """Train nerf model: data preparation, model and optimizer preparation, and model training."""
    md.set_seed(1)

    print(">>> Loading dataset")

    if config.dataset_type == "blender":
        images, poses, render_poses, hwf, i_split, test_ts, seq2ids_list, seq_id = load_blender_data(config.data_dir, config.half_res,
                                                                      config.test_skip, seq_num=config.seq_num)
        print("Loaded blender", images.shape, render_poses.shape, hwf, config.data_dir)
        i_train, i_val, i_test = i_split
        near = 2.0
        far = 6.0

        if config.white_bkgd:
            images = images[..., :3] * images[..., -1:] + (1.0 - images[..., -1:])
        else:
            images = images[..., :3]
        
        Ks = md.Tensor([[[hwf[2], 0, 0.5*hwf[1]],
                        [0, hwf[2], 0.5*hwf[0]],
                        [0, 0, 1]]])

    

    else:
        print("Unknown dataset type", config.dataset_type, "exiting")
        return

    if config.render_test:
        render_poses = poses[i_test.tolist()]

    print(f"TRAIN views: {i_train}\nTEST views: {i_test}\nVAL views: {i_val}")

    # Cast intrinsics to right types
    cap_h, cap_w, focal = hwf
    cap_h, cap_w = int(cap_h), int(cap_w)

    hwf = [cap_h, cap_w, focal]
    # Setup logging and directory for results
    print(">>> Saving checkpoints and results in", out_dir)
    # Create output directory if not existing

    os.makedirs(out_dir, exist_ok=True)
    # Record current configuration
    with open(os.path.join(out_dir, "configs.txt"), "w+", encoding="utf-8") as config_f:
        attrs = vars(config)
        for k in attrs:
            config_f.write(f"{k} = {attrs[k]}\n")

    # Create network models, optimizer and renderer
    print(">>> Creating models")

    # Create nerf model
    (
        start_iter,
        optimizer,
        model_coarse,
        model_fine,
        embed_fn,
        embed_dirs_fn,
        embedding_a, embedding_t, embedding_s,
    ) = create_nerf(config, out_dir)
    # Training steps
    global_steps = start_iter
    # Create volume renderer
    renderer = VolumeRenderer(
        config.chunk,
        config.cap_n_samples,
        config.cap_n_importance,
        config.net_chunk,
        config.white_bkgd,
        model_coarse,
        model_fine,
        embed_fn,
        embed_dirs_fn,
        config.encode_a,
        config.encode_t,
        embedding_a,
        embedding_t,
        embedding_s,
        near,
        far,
    )

    renderer_with_criterion = RendererWithCriterion(renderer, config, "nerfms", seq2ids_list)
    optimizer = md.nn.Adam(
        params=renderer.trainable_params(),
        learning_rate=config.l_rate,
        beta1=0.9,
        beta2=0.999,
    )

    train_renderer = md.nn.TrainOneStepCell(renderer_with_criterion, optimizer)
    train_renderer.set_train()

    # Start training
    print(">>> Start training")

    cap_n_rand = config.cap_n_rand

    # Move training data to GPU
    images = md.Tensor(images)
    poses = md.Tensor(poses)

    all_rays, all_rgbs, all_ts, all_st = get_rays(images, poses, Ks, i_train, seq_id)

    # Maximum training iterations
    cap_n_iters = config.cap_n_iters
    if start_iter >= cap_n_iters:
        return

    train_model(config, out_dir, images, poses, all_rays, all_rgbs, all_ts, all_st, i_test, test_ts, cap_h, cap_w, focal, start_iter, optimizer,
                global_steps, renderer, train_renderer, cap_n_rand, cap_n_iters)


def train_model(config, out_dir, images, poses, all_rays, all_rgbs, all_ts, all_st, i_test, test_ts, cap_h, cap_w, focal, start_iter, optimizer,
                global_steps, renderer, train_renderer, cap_n_rand, cap_n_iters):
    """Training model iteratively"""
    with tqdm(range(1, cap_n_iters + 1)) as p_bar:
        p_bar.n = start_iter
        start_sample = 0
        end_sample = 0
        for _ in p_bar:
            # Show progress
            p_bar.set_description(f"Iter {global_steps + 1:d}")
            p_bar.update()

            # Start time of the current iteration
            time_0 = time.time()

            end_sample = start_sample + cap_n_rand
            end_sample = end_sample if end_sample < all_rays.shape[0] else all_rays.shape[0]
            batch_rays = all_rays[start_sample:end_sample]
            target_s = all_rgbs[start_sample:end_sample]
            ts = all_ts[start_sample:end_sample]
            st = all_st[start_sample:end_sample]

            start_sample = end_sample if end_sample < all_rays.shape[0] else 0
            
            loss, psnr = train_net(config, global_steps, train_renderer, optimizer, batch_rays, target_s, ts, st)

            p_bar.set_postfix(time=time.time() - time_0, loss=loss, psnr=psnr)

            # Logging
            # Save training states
            if (global_steps + 1) % config.i_ckpt == 0:
                path = os.path.join(out_dir, f"{global_steps + 1:06d}.tar")

                md.save_checkpoint(
                    save_obj=renderer,
                    ckpt_file_name=path,
                    append_dict={"global_steps": global_steps},
                    async_save=True,
                )
                p_bar.write(f"Saved checkpoints at {path}")

            # Save testing results
            if (global_steps + 1) % config.i_testset == 0:
                test_save_dir = os.path.join(out_dir, f"test_{global_steps + 1:06d}")
                os.makedirs(test_save_dir, exist_ok=True)

                p_bar.write(f"Testing (iter={global_steps + 1}):")

                test_time, test_loss, test_psnr = test_net(
                    cap_h,
                    cap_w,
                    focal,
                    renderer,
                    md.Tensor(poses[i_test.tolist()]),
                    test_ts,
                    images[i_test.tolist()],
                    on_progress=lambda j, img: save_image(j, img, test_save_dir),  # pylint: disable=cell-var-from-loop
                    on_complete=lambda imgs: save_video(global_steps + 1, imgs, test_save_dir),  # pylint: disable=cell-var-from-loop
                )

                p_bar.write(
                    f"Testing results: [ Mean Time: {test_time:.4f}s, Loss: {test_loss:.4f}, PSNR: {test_psnr:.4f} ]")

            global_steps += 1


def main():
    """main function, set up config."""
    config = get_config()

    # Cuda device
    context_setup(config.gpu, config.device, getattr(md.context, config.mode))

    # Output directory
    base_dir = config.base_dir
    if not os.path.exists(base_dir):
        os.makedirs(base_dir)

    # Experiment name
    exp_name = config.dataset_type + "_" + config.name
    # get the experiment number
    exp_num = max([int(fn.split("_")[-1]) for fn in os.listdir(base_dir) if fn.find(exp_name) >= 0] + [0])
    if config.no_reload:
        exp_num += 1

    # Output directory
    out_dir = os.path.join(base_dir, exp_name + "_" + str(exp_num))

    # Start training pipeline
    train_pipeline(config, out_dir)


if __name__ == "__main__":
    main()
