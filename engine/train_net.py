# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""train step for nerf"""

import mindspore as md
from mindspore import nn

from engine import metrics
from engine.losses import *

__all__ = ["train_net", "RendererWithCriterion"]


def train_net(config, iter_, train_renderer, optimizer, rays, gt, ts, st):
    """
    Train a network.

    Args:
        config (Config): configuration.
        iter_ (int): current iterations.
        renderer (Callable): a volume renderer.
        optimizer (Optimizer): a network optimizer.
        rays (Tensor): a batch of rays for training. (#rays * #samples, 6)
        gt (Tensor): the ground truth.

    Returns:
        Tuple of 2 float, float], recorded metrics.
        - **loss** (float), loss to be recorded.
        - **psnr** (float), psnr to be recorded.
    """
    loss = train_renderer(rays, gt, ts, st)

    # Update learning rate
    decay_rate = 0.1
    decay_steps = config.l_rate_decay * 1000
    new_l_rate = config.l_rate * (decay_rate**(iter_ / decay_steps))
    optimizer.learning_rate = md.Parameter(new_l_rate)

    return float(loss), float(metrics.psnr_from_mse(train_renderer.network.mse))


class RendererWithCriterion(nn.Cell):
    """Renderer with criterion.

    Args:
        renderer (nn.Cell): renderer.
        loss_fn (nn.Cell, optional): loss function. Defaults to nn.MSELoss().

    Inputs:
        rays (Tensor): rays tensor.
        gt (Tensor): ground truth tensor.

    Outputs:
        Tensor, loss.
    """

    def __init__(self, renderer, config, loss_type="nerfms", seq2ids_list=None):
        """Renderer with criterion."""
        super().__init__()
        self.renderer = renderer
        self.loss_type = loss_type
        if loss_type == "nerfms":
            self.loss_fn = TripletLoss(seq2ids_list=seq2ids_list, embeddeds=renderer.embedding_a, opt=config)
        elif loss_type == "nerfw":
            self.loss_fn = NerfWLoss(config)
        else:
            self.loss_fn = nn.MSELoss()

    def construct(self, rays, gt, ts, st):
        """Renderer Trainer construct."""
        result = self.renderer(rays, ts, st)
        if not self.loss_type == "mse":
            loss, mse = self.loss_fn(result, gt)
            self.mse = mse
        else:
            loss = self.loss_fn(result["rgb_map_coarse"], gt) + self.loss_fn(result["rgb_map_fine"], gt)
            self.mse = loss
        return loss
    
    def backbone_network(self):
        """Return renderer."""
        return self.renderer
