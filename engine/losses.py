import random

from mindspore import nn
import mindspore as md

class MyTripletMarginLoss(nn.Cell):
    '''personal implementation of TripletMarginLoss, 
        since `RuntimeError: Illegal primitive TripletMarginLoss's bprop not defined`'''
    def __init__(self, margin):
        super().__init__()

        self.margin = margin

    def construct(self, anchor, positive, negative):
        pos_dist = (anchor - positive).pow(2).sum(1) 
        neg_dist = (anchor - negative).pow(2).sum(1) 
        loss = md.ops.ReLU()(pos_dist - neg_dist + self.margin) 
        return loss.mean()



class TripletLoss(nn.Cell):
    def __init__(self, seq2ids_list, embeddeds, opt):
        super().__init__()
        self.opt = opt
        self.nerfw_loss = NerfWLoss(opt)
        self.sequence_num = len(seq2ids_list)
        self.sequence_label = list(range(self.sequence_num))
        self.seq2ids_list = seq2ids_list
        self.triplet_weight = opt.triplet_weight
        self.embeddeds = embeddeds
        self.margin = opt.margin
        self.triplet_loss = MyTripletMarginLoss(margin=self.margin)



    def construct(self, inputs, targets):
        triplet_loss = 0
        for i in range(self.sequence_num):
            anchor_idx = self.seq2ids_list[i]
            positive_idx = self.get_positive_index(anchor_idx)
            negative_idx = self.get_negative_index(anchor_idx, i)
            anchor_latents = self.embeddeds(anchor_idx)
            triplet_loss += self.triplet_weight * self.triplet_loss(anchor_latents, self.embeddeds(positive_idx), self.embeddeds(negative_idx)) + self.opt.kl_weight * anchor_latents.pow(2).mean()
        rgb_loss, mse = self.nerfw_loss(inputs, targets)
        loss = rgb_loss +  triplet_loss / self.sequence_num
        return loss, mse

    def get_positive_index(self, anchor_idx):
        return anchor_idx[md.ops.randint(0, anchor_idx.shape[0], size=(anchor_idx.shape[0],))]

    def get_negative_index(self, anchor_idx, anchor_label):
        negative_label = (anchor_label + random.randint(1, self.sequence_num-1)) % self.sequence_num
        negative_idx_list =  self.seq2ids_list[negative_label]
        return negative_idx_list[md.ops.randint(0, negative_idx_list.shape[0], size=(anchor_idx.shape[0],))]


class NerfWLoss(nn.Cell):

    def __init__(self, opt, coef=1, lambda_u=0.01):
        super().__init__()

        self.coef = coef
        self.lambda_u = lambda_u
        self.opt = opt


    def construct(self, inputs, targets):
        ret = {}
        ret['c_l'] = 0.5 * ((inputs['rgb_map_coarse']-targets)**2).mean()
        if 'rgb_map_fine' in inputs:
            if 'beta' not in inputs: # no transient head, normal MSE loss
                mse = (inputs['rgb_map_fine']-targets)**2
                ret['f_l'] = 0.5 * mse.mean()
            else:
                #ret['f_l'] = 0.5 * ((inputs['rgb_fine']-targets)**2).mean()
                mse = (inputs['rgb_map_fine']-targets)**2
                ret['f_l'] = \
                     (mse/(2*inputs['beta'].unsqueeze(1)**2)).mean()
                ret['b_l'] = 3 + md.ops.log(inputs['beta']).mean() # +3 to make it positive
                ret['s_l'] = self.lambda_u * inputs['transient_sigmas'].mean() 
        
        loss_d = 0
        for k, v in ret.items():
            loss_d += self.coef * v

        return loss_d, mse.mean()

