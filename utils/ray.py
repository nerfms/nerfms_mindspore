# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""nerf ray generator"""

import mindspore as md
import mindspore.numpy as mnp
import time
__all__ = ["generate_rays", "get_rays"]


def generate_rays(h, w, f, pose):
    """
    Given an image plane, generate rays from the camera origin to each pixel on the image plane.

    Args:
        h (int): height of the image plane.
        w (int): width of the image plane.
        f (int): focal length of the image plane.
        pose (Tensor): the extrinsic parameters of the camera. (3, 4) or (4, 4).

    Returns:
        Tuple of 2 tensor, origins of rays and directions of rays.
        - **rays_origins** (Tensor), origins of rays.
        - **ray_dirs** (Tensor), directions of rays.
    """

    # Coordinates of the 2D grid
    f = md.Tensor(f, dtype=md.float32)
    cols = md.ops.ExpandDims()(md.numpy.linspace(-1.0 * w / 2, w - 1 - w / 2, w) / f, 0).repeat(h, axis=0)
    rows = md.ops.ExpandDims()(-1.0 * md.numpy.linspace(-1.0 * h / 2, h - 1 - h / 2, h) / f, 1).repeat(w, axis=1)

    # Ray directions for all pixels
    ray_dirs = md.numpy.stack([cols, rows, -1.0 * md.numpy.ones_like(cols)], axis=-1)  # (h, w, 3)
    # Apply rotation transformation to make each ray orient according to the camera
    unsqueeze_op = md.ops.ExpandDims()
    ray_dirs = md.numpy.sum(unsqueeze_op(ray_dirs, 2) * pose[:3, :3], axis=-1)
    # Origin position
    rays_origins = pose[:3, -1].expand_as(ray_dirs)  # (h, w, 3)

    return rays_origins.astype("float32"), ray_dirs.astype("float32")  # (h, w, 3), (h, w, 3)

def get_rays(images, poses, Ks, index_list, seq_id):
    all_rays = []
    all_rgbs = []
    all_ts = []
    all_st = []
    index_list = index_list.tolist()
    for idx in index_list:
        H, W, _ = images[idx].shape
        
        if Ks.shape[0] > 1:
            K = Ks[idx]
        else:
            K = Ks[0]
    
        i, j = mnp.meshgrid(mnp.arange(W, dtype=md.float32), mnp.arange(H, dtype=md.float32), indexing='xy')
        # Ray directions for all pixels
        ray_dirs = mnp.stack([(i-K[0][2])/K[0][0], -(j-K[1][2])/K[1][1], -1.0 * md.numpy.ones_like(i)], axis=-1)  # (h, w, 3)
        # Apply rotation transformation to make each ray orient according to the camera
        unsqueeze_op = md.ops.ExpandDims()
        ray_dirs = md.numpy.sum(unsqueeze_op(ray_dirs, 2) * poses[idx, :3, :3], axis=-1)
        
        # Origin position
        rays_origins = poses[idx, :3, -1].expand_as(ray_dirs)  # (h, w, 3)
        all_rays += [mnp.stack([rays_origins.view((-1, 3)), ray_dirs.view((-1, 3))], axis=1)] # (N*h*w, 2, 3)
        all_rgbs += [images[idx].view((-1, 3))]
        all_ts += [idx * mnp.ones((H*W, 1), dtype=md.int64)]
        all_st += [seq_id[idx] * mnp.ones((H*W, 1), dtype=md.int64)]

    all_rays = md.ops.concat(all_rays, axis=0) #(h*w*N, 2, 3)
    all_rgbs = md.ops.concat(all_rgbs, axis=0) #(h*w*N, 3)

    all_ts = md.ops.concat(all_ts, axis=0) #(h*w*N, 1)
    all_st = md.ops.concat(all_st, axis=0)

    #random shuffle
    rnd_idx = md.ops.Randperm(max_length=all_rays.shape[0])(md.Tensor([all_rays.shape[0]], md.int32))
    all_rays = all_rays[rnd_idx]
    all_rgbs = all_rgbs[rnd_idx]
    all_ts = all_ts[rnd_idx]
    all_st = all_st[rnd_idx]
    return all_rays, all_rgbs, all_ts, all_st