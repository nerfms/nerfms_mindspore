# Copyright 2022 Huawei Technologies Co., Ltd
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ============================================================================
"""nerf mlp"""

import mindspore.ops.operations as P
from mindspore import nn
import mindspore as md
__all__ = ["NeRFMLP"]


class NeRFMLP(nn.Cell):
    """
    NeRF MLP architecture. Implementation according to the official code release
    <https://github.com/bmild/nerf/blob/master/run_nerf_helpers.py#L104-L105>.

    Args:
        cap_d (int, optional): model depth. Defaults to 8.
        cap_w (int, optional): model width. Defaults to 256.
        input_ch (int, optional): input channel. Defaults to 3.
        input_ch_views (int, optional): input view channel. Defaults to 3.
        output_ch (int, optional): output channel. Defaults to 4.
        skips (tuple, optional): skip connection layer index. Defaults to (4).
        use_view_dirs (bool, optional): use view directions or not. Defaults to False.

    Inputs:
        x (Tensor): query tensors: points and view directions (..., 6).

    Outputs:
        Tensor, query features (..., feature_dims).
    """

    def __init__(
            self,
            typ,
            cap_d=8,
            cap_w=256,
            input_ch=3,
            input_ch_views=3,
            output_ch=4,
            skips=(4),
            encode_appearance=False,
            encode_transient=False,
            in_channel_a=48,
            in_channel_t=16,
            in_channel_s=16,
            beta_min=0.03
    ):
        super().__init__()
        self.cap_d = cap_d
        self.cap_w = cap_w
        self.input_ch = input_ch
        self.input_ch_views = input_ch_views
        self.skips = skips

        # NeRF-in-the-Wild
        self.encode_appearance = False if typ=='coarse' else encode_appearance
        self.encode_transient = False if typ=='coarse' else encode_transient
        self.in_channels_a = in_channel_a if self.encode_appearance else 0
        self.in_channels_t = in_channel_t if self.encode_transient else 0
        self.in_channels_s = in_channel_s if self.encode_transient else 0
        self.beta_min = beta_min


        self.pts_linears = nn.CellList([nn.Dense(in_channels=input_ch, out_channels=cap_w)] + [
            nn.Dense(in_channels=cap_w, out_channels=cap_w) if i not in
            self.skips else nn.Dense(in_channels=cap_w + input_ch, out_channels=cap_w) for i in range(cap_d - 1)
        ])
        
        self.views_linears = nn.CellList([nn.Dense(in_channels=input_ch_views + cap_w + self.in_channels_a, out_channels=cap_w // 2)])

        self.feature_linear = nn.Dense(in_channels=cap_w, out_channels=cap_w)
        self.alpha_linear = nn.Dense(in_channels=cap_w, out_channels=1)
        self.rgb_linear = nn.Dense(in_channels=cap_w // 2, out_channels=3)
        
        if self.encode_transient:
            self.s_transient_encoding = nn.SequentialCell([
                nn.Dense(in_channels=cap_w+in_channel_s, out_channels=cap_w//2), nn.ReLU(),
                nn.Dense(in_channels=cap_w//2, out_channels=cap_w), nn.ReLU()
            ])
            self.transient_encoding = nn.SequentialCell([
                nn.Dense(in_channels=cap_w+in_channel_t, out_channels=cap_w//2), nn.ReLU(),
                nn.Dense(in_channels=cap_w//2, out_channels=cap_w//2), nn.ReLU()
            ])
            self.transient_sigma = nn.Dense(cap_w//2, 1)
            self.transient_rgb = nn.Dense(cap_w//2, 3)
            self.transient_beta = nn.Dense(cap_w//2, 1)
    
    def freeze(self):
        pass

    def unfreeze(self):
        pass
    
    def construct(self, x, sigma_only=False, output_transient=True):
        """NeRF MLP construct"""
        if sigma_only:
            input_pts = x
        elif output_transient:
            
            input_pts, input_dir_a, input_t, input_s = md.ops.split(x, [self.input_ch,
                                                                self.input_ch_views+self.in_channels_a,
                                                                self.in_channels_t, self.in_channels_s], axis=-1)
        else:
            input_pts, input_dir_a = md.ops.split(x, [self.input_ch,
                                                    self.input_ch_views+self.in_channels_a], axis=-1)
        
        h = input_pts
        for i, _ in enumerate(self.pts_linears):
            h = self.pts_linears[i](h)
            h = P.ReLU()(h)
            if i in self.skips:
                h = P.Concat(-1)([input_pts, h])

        alpha = self.alpha_linear(h)
        if sigma_only:

            return alpha
        feature = self.feature_linear(h)
        h = P.Concat(-1)([feature, input_dir_a])

        for i, _ in enumerate(self.views_linears):
            h = self.views_linears[i](h)
            h = P.ReLU()(h)

        rgb = self.rgb_linear(h)
        outputs = P.Concat(-1)([rgb, alpha])
        if not output_transient:
            return outputs
        
        transient_encoding_input = P.Concat(-1)([feature, input_s])
        # h = transient_encoding_input
        # for i, _ in enumerate(self.transient_encoding):
        #     h = self.transient_encoding[i](h)
        # transient_encoding = h
        transient_encoding = feature + self.s_transient_encoding(transient_encoding_input)
        transient_encoding = P.Concat(-1)([transient_encoding, input_t])
        transient_encoding = self.transient_encoding(transient_encoding)
        transient_sigma = P.Softplus()(self.transient_sigma(transient_encoding)) # (B, 1)
        transient_rgb = P.Sigmoid()(self.transient_rgb(transient_encoding)) # (B, 3)
        transient_beta = P.Softplus()(self.transient_beta(transient_encoding)) # (B, 1)

        transient = P.Concat(-1)([transient_rgb, transient_sigma,
                               transient_beta]) # (B, 5)

        outputs = P.Concat(-1)([outputs, transient])
        return outputs
